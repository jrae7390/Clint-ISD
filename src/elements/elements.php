<?php 
$elementJson = trim($_POST['elementJson']);

//CREATE THE TEMPLATE SKELETON FILES
$elementFile = fopen("../template/elements/elements.json", "w") or die("Unable to open file!");
fwrite($elementFile, $elementJson);
fclose($elementFile);

echo json_encode(array("msg" => "Your Element File has been generated"));
?>